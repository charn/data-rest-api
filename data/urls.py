from rest_framework import routers

from data.views import DataViewSet

app_name = "data"

router = routers.SimpleRouter()

router.register(r"data", DataViewSet)

urlpatterns = router.urls
