from django.contrib.postgres.fields import JSONField
from django.core.serializers.json import DjangoJSONEncoder
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Data(models.Model):
    created_at = models.DateTimeField(
        verbose_name=_("created at"), auto_now_add=True, editable=False
    )
    modified_at = models.DateTimeField(
        verbose_name=_("modified at"), auto_now=True, editable=False
    )
    application = models.CharField(max_length=255, verbose_name=_("application"))
    source = models.CharField(max_length=255, verbose_name=_("source"))
    data = JSONField(
        verbose_name=_("attribute data"), default=dict, encoder=DjangoJSONEncoder
    )

    class Meta:
        verbose_name = _("data")
        verbose_name_plural = _("data")
        ordering = ("id",)
