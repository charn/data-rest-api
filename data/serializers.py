from rest_framework import serializers

from data.models import Data


class DataSerializer(serializers.ModelSerializer):
    class Meta:
        model = Data
        fields = ["id", "created_at", "modified_at", "application", "source", "data"]
        read_only_fields = ("created_at", "modified_at")
