from rest_framework import viewsets

from data.models import Data
from data.serializers import DataSerializer


class DataViewSet(viewsets.ModelViewSet):
    queryset = Data.objects.all()
    serializer_class = DataSerializer
    filter_fields = ("application", "source")

    def get_queryset(self):
        qs = super().get_queryset()
        qs.order_by("id")
        return qs
