FROM python:3.6.7-slim-stretch

ENV PYTHONUNBUFFERED 1

RUN mkdir /code
RUN mkdir /entrypoint
WORKDIR /code

# Install the appropriate Ubuntu packages
RUN apt-get update && apt-get install -y \
    build-essential \
    libpq-dev \
    git \
    binutils \
    netcat

# Upgrade pip
RUN pip install -U pip

# Install python dependencies
ADD requirements.txt /code/
ADD requirements-dev.txt /code/

RUN pip install --no-cache-dir -r requirements.txt
RUN pip install --no-cache-dir -r requirements-dev.txt

# Add entrypoint script
ADD docker-entrypoint.sh /entrypoint/
RUN chmod +x /entrypoint/docker-entrypoint.sh

#ADD . /code/
#ENTRYPOINT /entrypoint/docker-entrypoint.sh
